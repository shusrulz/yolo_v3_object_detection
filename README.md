## Leveraging YOLO v3 as python package
You only look once (YOLO) is a state-of-the-art, real-time object detection system. On a Pascal Titan X it processes images at 30 FPS and has a mAP of 57.9% on COCO test-dev.

## Performance of YOLO v3
YOLOv3 is extremely fast and accurate. In mAP measured at .5 IOU YOLOv3 is on par with Focal Loss but about 4x faster. Moreover, you can easily tradeoff between speed and accuracy simply by changing the size of the model, no retraining required!

One can read the full paper here https://arxiv.org/pdf/1804.02767.pdf

## Weights and configuration from the pretrained models are
- yolov3.weights
- coco.names
- yolov3.cfg

## Instruction to test YOLOv3
- Run python test.py on terminal